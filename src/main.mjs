import fs from 'fs';
import cheerio from 'cheerio';
import axios from 'axios';
import logger from './logger.mjs';

const SDN_URL = 'https://www.treasury.gov/ofac/downloads/sdn.xml';

(async () => {
  logger.info('start fetching sdn.xml');
  const response = await axios.get(SDN_URL, {
    responseType: 'text',
  });
  logger.info('finish fetch sdn.xml');
  const tree = cheerio.load(response.data);
  const domains = tree('sdnEntry > idList > id')
    .filter((_, ele) => tree(ele).find('idType').text() === 'Website')
    .map((_, ele) => {
      let urlId = tree(ele).find('idNumber').text();
      if (!urlId.startsWith('http')) {
        urlId = `https://${urlId}`;
      }
      const { hostname } = new URL(urlId);
      return hostname.replace(/^www\./, '');
    })
    .toArray();
  const content = JSON.stringify({ domains }, null, 2);
  fs.writeFile('public/domains.json', content, 'utf8', () => {
    logger.info('finish create domains.json');
    process.exit();
  });
})();
